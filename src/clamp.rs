pub mod clamp
{
    use std::{time};
    //use std::{thread, time};
    //use std::io;
    use ev3dev_lang_rust::sensors::{SensorPort, ColorSensor};
    use ev3dev_lang_rust::motors::{MotorPort, MediumMotor, LargeMotor};
    use ev3dev_lang_rust::{Ev3Result};

    #[derive(PartialEq, Eq)]
    enum LiftStatus
    {
        UP,
        HALF,
        DOWN
    }

    #[derive(PartialEq, Eq)]
    enum ClampStatus
    {
        OPENED,
        MOVING,
        CLOSED
    }

    struct ClampSystem
    {
        motor: MediumMotor,
        status: ClampStatus
    }
    struct LiftSystem
    {
        motor: LargeMotor,
        status: LiftStatus,
        rel_pos: i32
    }
    struct ColorSystem
    {
        sensor: ColorSensor
    }
    pub struct PickingSystem
    {
        scissor_lift: LiftSystem,
        clamp: ClampSystem,
        color_detector: ColorSystem
    }
    
    

    // Methods
    impl PickingSystem
    {
        pub fn new(lifting_motor_port: MotorPort, clamp_motor_port: MotorPort, colorsensor_port: SensorPort) -> Self
        {
            PickingSystem
            {
                scissor_lift: LiftSystem::new(lifting_motor_port),
                clamp: ClampSystem::new(clamp_motor_port),
                color_detector: ColorSystem::new(colorsensor_port)
            }
        }

        ///Lifts up the clamp
        pub fn up(&mut self) -> Ev3Result<()>
        {
            self.scissor_lift.up()?;
            self.scissor_lift.up()?;
            Ok(())
        }

        ///Lifts down the clamp
        /*pub fn down(&mut self) -> Ev3Result<()>
        {
            self.scissor_lift.down()?;
            self.scissor_lift.down()?;
            Ok(())
        }*/

        ///Picking Mechanism
        pub fn try_pick(&mut self) -> Ev3Result<bool>
        {
            self.clamp.close()?;
            self.scissor_lift.down()?;
            self.clamp.open()?;
            self.scissor_lift.down()?;

            self.clamp.close()?;

            self.scissor_lift.up()?;
            self.scissor_lift.up()?;

            return self.color_detector.ball_present();
        }
 
        ///Open the clamp to release the ball
        pub fn release(&mut self) -> Ev3Result<()>
        {
            self.clamp.open()?;
            Ok(())
        }

        ///reset the clamp after release
        pub fn reset(&mut self) -> Ev3Result<()>
        {
            self.clamp.close()?;
            Ok(())
        }
    }

    impl LiftSystem
    {
        fn new(lifting_port: MotorPort) -> Self
        {
            // Initialize the motor parameters
            let motor = LargeMotor::get(lifting_port).unwrap();
            motor.reset().unwrap();
            motor.set_polarity(LargeMotor::POLARITY_NORMAL).unwrap();
            motor.set_position(0).unwrap();

            let max_speed = motor.get_max_speed().unwrap() as f32;
            motor.set_stop_action(LargeMotor::STOP_ACTION_BRAKE).unwrap();
            motor.set_speed_sp(
                (max_speed*1.0) as i32
            ).unwrap();


            motor.set_ramp_up_sp(1000).unwrap();
            motor.set_ramp_down_sp(1000).unwrap();

            // //DEBUG calibration
            // println!("Input wanted height: ");

            // let mut wanted = String::new();

            // io::stdin()
            //     .read_line(&mut wanted)
            //     .expect("Failed to read line");
            // let height = wanted.trim().parse::<i32>().unwrap();
            let height: i32 = 2560;
            LiftSystem 
            {
                motor: motor,
                status: LiftStatus::DOWN,
                rel_pos: height
            }
        }

        //scissor liftup
        fn up(&mut self) -> Ev3Result<()>
        {  
            if self.status == LiftStatus::UP
            {
                return Ok(());
            }

            self.motor.run_to_rel_pos(Some(self.rel_pos/2) )?;
            self.motor.wait_until_not_moving(None);
            
            self.status = match self.status {
                LiftStatus::DOWN => LiftStatus::HALF,
                LiftStatus::HALF => LiftStatus::UP,
                _ => LiftStatus::UP
            };
            Ok(())
        }

        //scissor liftdown
        fn down(&mut self) -> Ev3Result<()>
        {
            if self.status == LiftStatus::DOWN
            {
                return Ok(());
            }

            self.motor.run_to_rel_pos( Some(-self.rel_pos/2) )?;
            self.motor.wait_until_not_moving(None);

            self.status = match self.status {
                LiftStatus::UP => LiftStatus::HALF,
                LiftStatus::HALF => LiftStatus::DOWN,
                _ => LiftStatus::DOWN
            };
            Ok(())
        }

    }

    impl ClampSystem
    {
        fn new(clamp_port: MotorPort) -> Self
        {   
            //Initialise the motor parameters 
            let motor = MediumMotor::get(clamp_port).unwrap();
            motor.reset().unwrap();
            let max_speed = motor.get_max_speed().unwrap() as f32;
            motor.set_stop_action(LargeMotor::STOP_ACTION_BRAKE).unwrap();
            motor.set_speed_sp(
                (max_speed*0.2) as i32
            ).unwrap();
            motor.set_position(0).unwrap();

            ClampSystem 
            {
                motor: motor,
                status: ClampStatus::CLOSED
            }
        }

        //close the clamp
        fn close(&mut self) -> Ev3Result<()>
        {
            if self.status == ClampStatus::CLOSED
            {
                return Ok(());
            }

            if self.status == ClampStatus::OPENED
            {
                self.motor.run_to_abs_pos( Some(0) )?;
                self.status = ClampStatus::MOVING;
            }
            self.motor.wait_until_not_moving( Some(time::Duration::from_secs(4)) );
            self.status = ClampStatus::CLOSED;
            self.motor.stop()?;
            Ok(())
        }

        //Open the clamp
        fn open(&mut self) -> Ev3Result<()>
        {
            if self.status == ClampStatus::OPENED
            {
                return Ok(());
            }

            if self.status == ClampStatus::CLOSED
            {
                self.motor.run_to_abs_pos( Some(400) )?;
                self.status = ClampStatus::MOVING;
            }
            self.motor.wait_until_not_moving( Some(time::Duration::from_secs(4)) );
            self.status = ClampStatus::OPENED;
            self.motor.stop()?;
            Ok(())
        }
    }

    //Detect the ball based on the color
    impl ColorSystem 
    {
        fn new(port: SensorPort) -> Self
        {
            let sensor = ColorSensor::get(port).unwrap();
            sensor.set_mode(ColorSensor::MODE_COL_COLOR).unwrap();
            ColorSystem
            {
                sensor: sensor
            }
        }

        fn ball_present(&self) -> Ev3Result<bool>
        {
            return match self.sensor.get_value0()? {
                2 => Ok(true),//Blue
                5 => Ok(true),//Red
                _ => Ok(false)//Others
            };
        }
    }
}
