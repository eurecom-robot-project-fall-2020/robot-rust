extern crate ev3dev_lang_rust;

use ev3dev_lang_rust::motors::MotorPort;
use ev3dev_lang_rust::sensors::SensorPort;
//use ev3dev_lang_rust::sensors::CompassSensor;
use ev3dev_lang_rust::Ev3Result;
//use std::{thread, time};
mod clamp;
mod navigation;

fn main() -> Ev3Result<()> {

    
    // TEST COMPASS

   /* let mut n = navigation::Navigation::new(
        MotorPort::OutC,
        MotorPort::OutB,
        SensorPort::In2,
        SensorPort::In1,
        SensorPort::In3,  // check the compass sensor port
    );

    let mut compass = CompassSensor::get(SensorPort::In3).unwrap();
    thread::sleep(time::Duration::from_millis(2000));
    compass.set_zero().unwrap();
    compass.start_calibration().unwrap();
    thread::sleep(time::Duration::from_millis(300));
    n.rotate_angle(360)?;
    thread::sleep(time::Duration::from_millis(300));
    compass.stop_calibration().unwrap();

    thread::sleep(time::Duration::from_millis(300));
    for _i in 1..1000 {
        println!("Compass value absolute: {}", compass.get_value0().unwrap());
        println!("Compass value relative: {}", compass.get_relative_rotation().unwrap());
        thread::sleep(time::Duration::from_millis(300));
    }
    return Ok(());
    */

    let mut n = navigation::Navigation::new(
        MotorPort::OutC,
        MotorPort::OutB,
        SensorPort::In2,
        SensorPort::In1,
        //SensorPort::In3,  // compass 
    );
 
 
    //Count balls
    let mut ball_count = 0;
    //Picking
    let mut p = clamp::clamp::PickingSystem::new(MotorPort::OutD, MotorPort::OutA, SensorPort::In4);
    //lift the clamp
    p.up()?;

    // Calibrate the compass sensor
    //n.compass_calibration()?; 

    // move from the starting point
    n.move_t(100.0,10.0)?;

    // rotate right facing the fence
    n.rotate_angle(90)?;

    //move forward checking distance with the fence(24cm away from fence)
    n.move_t(100.0,24.0)?;

    //Rotate left to face the box
    n.rotate_angle(-90)?;
    
    // correcting the angle using gyro
    n.reset_angle()?;

    //Check distance from box: 15cm
    n.move_t(40.0,15.0)?;

    //We stop 1cm away from the box
    n.move_t(14.0,0.0)?;


    //Try to pick up the ball

    loop {
        if p.try_pick()? {
            break;
        } else {
            println!("Ball not found");
        }
    }

    //Take route1 to cube while ball_count<=2
    while ball_count < 4 
    {
        println!("Ball_count:{}",ball_count);
        while ball_count <= 2 
        {
            // move 35cm backward
            n.move_t(-35.0,0.0)?;

            // rotate left 
            n.rotate_angle(-90)?;
            
            // correcting the angle using gyro
            n.reset_angle()?;

            //move forward checking distance with the fence(24cm away from fence)
            n.move_t(150.0,24.0)?;

            //Rotate right to face the box
            n.rotate_angle(90)?;

            // correcting the angle using gyro
            n.reset_angle()?;

            //Check distance from box
            n.move_t(40.0,15.0)?;

            //We stop 1cm away from the box
            n.move_t(14.0,0.0)?;

            //release the ball
            p.release()?;
            ball_count = ball_count + 1;

            // reset the clamp
            p.reset()?;

            //Going back to source cube
            
            //move backward from the box
            n.move_t(-10.0,0.0)?;
           
            //rotate right
            n.rotate_angle(90)?;
            
            // correcting the angle using gyro
            n.reset_angle()?;

            //move forward checking distance with the fence(21cm away from fence)
            n.move_t(120.0,24.0)?;

            //Turn left to face the box
            n.rotate_angle(-90)?;
           
            // correcting the angle using gyro
            n.reset_angle()?;

            //Check distance from box: 15cm
            n.move_t(40.0,15.0)?;

            //We stop 1cm away from the box
            n.move_t(14.0,0.0)?;

            // picking system
            loop {
                if p.try_pick()? {
                    break;
                } else {
                    println!("Ball not found");
                }
            }
        }


        //Now ball_count==3 put in the destination cube, now take route2 to put the last ball

        // move backward by 65 cm to allign with the pyramid
        n.move_t(-65.0,0.0)?;

        // rotate left to face pyramid
        n.rotate_angle(-90)?;
            
        // correcting the angle using gyro
        n.reset_angle()?;

        //Check distance from box: 15cm
        n.move_t(100.0,15.0)?;
     
        //We stop 1cm away from the box
        n.move_t(14.0,0.0)?;

        // release the ball
        p.release()?;
        ball_count = ball_count + 1;
 
        // reset the clamp
        p.reset()?;

        //Go back to source cube

        // move back by 40 cm
        n.move_t(-30.0,0.0)?;
 
        // rotate 180 to face the fence
        n.rotate_angle(-180)?;
        
        // correcting the angle using gyro
        n.reset_angle()?;

        //move forward checking distance with the fence(21cm away from fence)
        n.move_t(120.0,24.0)?;

        //rotate left to face the box
        n.rotate_angle(-90)?;
        
        // correcting the angle using gyro
        n.reset_angle()?;

        //Check distance from box: 15cm
        n.move_t(80.0,15.0)?;

        //We stop 1cm away from the box
        n.move_t(14.0,0.0)?;

        //picking system
        loop {
            if p.try_pick()? {
                break;      
                  } else {
            println!("Ball not found");
            }
       }


    }

     println!("Got it! Exiting...");
     n.stop()?;  

    Ok(())
}
