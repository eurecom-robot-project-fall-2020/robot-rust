use ev3dev_lang_rust::motors::{LargeMotor, MotorPort};
//use ev3dev_lang_rust::sensors::{CompassSensor, GyroSensor, SensorPort, UltrasonicSensor};
use ev3dev_lang_rust::sensors::{GyroSensor, SensorPort, UltrasonicSensor};
use ev3dev_lang_rust::Ev3Result;
use std::f32::consts::PI;
use std::{thread, time};
use std::sync::mpsc;
use std::sync::mpsc::{TryRecvError};

pub struct Navigation {
    motor_l: LargeMotor,
    motor_r: LargeMotor,
    gyro_sensor: GyroSensor,
    distance_sensor_port: SensorPort,
    wheel_radius_cm: f32,
    counts_per_rot: i32,
}

impl Navigation {
    pub fn new(
        port_motor_l: MotorPort,
        port_motor_r: MotorPort,
        gyro_port: SensorPort,
        ultrasonic_port: SensorPort,
        //compass_port: SensorPort,
    ) -> Self {

        //Initialize motor parameters
      
        let motor_l = LargeMotor::get(port_motor_l).unwrap();
        let motor_r = LargeMotor::get(port_motor_r).unwrap();
       
        // get tachocounts per rotation 
        let counts_per_rot = motor_l.get_count_per_rot().unwrap();

        motor_l.reset().unwrap();
        motor_r.reset().unwrap();

        // set the speed and the break action to the motors
        let max_speed = motor_l.get_max_speed().unwrap() as f32;
        motor_l
            .set_stop_action(LargeMotor::STOP_ACTION_BRAKE)
            .unwrap();
        motor_l.set_speed_sp((max_speed * 0.5) as i32).unwrap();
        motor_r
            .set_stop_action(LargeMotor::STOP_ACTION_BRAKE)
            .unwrap();
        motor_r.set_speed_sp((max_speed * 0.5) as i32).unwrap();

        // set the polarity of the motors
        motor_l.set_polarity(LargeMotor::POLARITY_NORMAL).unwrap();
        motor_r.set_polarity(LargeMotor::POLARITY_NORMAL).unwrap();

        //set rampup and ramp down time to the motors
        motor_l.set_ramp_up_sp(1000).unwrap();
        motor_r.set_ramp_up_sp(1000).unwrap();
        motor_l.set_ramp_down_sp(1000).unwrap();
        motor_r.set_ramp_down_sp(1000).unwrap();

        //Initialize the gyro sensor
        let gyro = GyroSensor::get(gyro_port).unwrap();
        gyro.set_mode_col_rate().unwrap();
        gyro.set_mode_col_ang().unwrap();

        //Initialize the compass sensor
        //let compass = CompassSensor::get(compass_port).unwrap();
        //thread::sleep(time::Duration::from_millis(500));
        /*thread::sleep(time::Duration::from_millis(500));
        let rel_rotation = compass.get_relative_rotation().unwrap();
        println!("rel_rotation angle {}", rel_rotation);*/

        Navigation {
            motor_l: motor_l,
            motor_r: motor_r,
            gyro_sensor: gyro,
            distance_sensor_port: ultrasonic_port,
            wheel_radius_cm: 2.75,
            counts_per_rot: counts_per_rot,
        }
    }

    ///calibrate the compass sensor by rotating 360 degrees
    /*pub fn compass_calibration(&mut self) -> Ev3Result<()> {

        self.compass_sensor.start_calibration()?;
        thread::sleep(time::Duration::from_millis(300));
        self.rotate_angle(360)?;
        thread::sleep(time::Duration::from_millis(300));
        self.compass_sensor.stop_calibration()?;
        self.compass_sensor.set_zero()?;
        Ok(())
    }*/

    /// convert tachocount to radians
    fn tacho_to_radians(&self, tacho: i32) -> f32 {
        let tacho = tacho as f32;
        let counts_per_rot = self.counts_per_rot as f32;
        2.0 * PI * tacho / counts_per_rot
    }

    /// Move_t  taking into consideration both tacho and ultrasonic sensor
    ///
    /// distance_cm is the limit distance that will be used for tacho_counts
    /// distance_sensor_limi is the limit distance used for the ultrasonic sensor comparison
    ///
    /// If distance_sensor_limit is 0 or negative, the ultrasonic sensor is ignored
    /// This is useful, for example, when moving backward or when approaching a box
    pub fn move_t(&self, distance_cm: f32, distance_sensor_limit: f32) -> Ev3Result<()> {
        #[derive(PartialEq, Eq, Debug)]
        enum TripDistance {
            ARRIVED,
            CLOSE,
            RUNNING,
        }

        self.motor_l.set_polarity(LargeMotor::POLARITY_NORMAL)?;
        self.motor_r.set_polarity(LargeMotor::POLARITY_NORMAL)?;

        //set the tachocounts of the motor to 0
        self.motor_l.set_position(0)?;
        self.motor_r.set_position(0)?;

        //get starting position of gyro
        let starting_pos = self.gyro_sensor.get_value0()?;

        //Start actual motor run
        self.motor_l.set_duty_cycle_sp(0)?;
        self.motor_r.set_duty_cycle_sp(0)?;
        self.motor_l.run_direct()?;
        self.motor_r.run_direct()?;

        let mut trip_status = TripDistance::RUNNING;

        let (tx_ultrasonic, rx_ultrasonic) = mpsc::channel();
        let port = self.distance_sensor_port.clone();

        thread::spawn(move || {
            let distance_sensor = UltrasonicSensor::get(port).unwrap();
            loop {
                let a = distance_sensor.get_distance_centimeters().unwrap();
                match tx_ultrasonic.send(a)
                {
                    Ok(_) => (),
                    Err(_) => break
                };
                thread::sleep(time::Duration::from_millis(150));
            }
        });

        while trip_status != TripDistance::ARRIVED {
            // Get values from the distance sensor thread using a Rust channel,
            // so we can run the sensor at a different frequency
            let curr_distance = match rx_ultrasonic.try_recv(){
                Ok(v) => v,
                Err(TryRecvError::Empty) => 250.0,
                Err(TryRecvError::Disconnected) => break
            };

            //check if the distance of the object detected is with in the range and assign the trip state 
            if distance_sensor_limit >= 3.0 && curr_distance < 200.0 {
                if trip_status == TripDistance::RUNNING {
                    if curr_distance < distance_sensor_limit + 10.0 {
                        trip_status = TripDistance::CLOSE;
                    }
                } else if trip_status == TripDistance::CLOSE {
                    if curr_distance <= distance_sensor_limit {
                        trip_status = TripDistance::ARRIVED;
                    }
                }
            }

        
            //Tacho counts condition
            let curr_cm_done = (self.tacho_to_radians(self.motor_l.get_position()?)
                * self.wheel_radius_cm
                + self.tacho_to_radians(self.motor_l.get_position()?) * self.wheel_radius_cm)
                / 2.0;
            //println!("Current tacho cm {}", curr_cm_done);
            if trip_status == TripDistance::RUNNING {
                if curr_cm_done.abs() > distance_cm.abs() - 10.0 {
                    trip_status = TripDistance::CLOSE;
                }
            } else if trip_status == TripDistance::CLOSE {
                if curr_cm_done.abs() >= distance_cm.abs() {
                    trip_status = TripDistance::ARRIVED;
                }
            }

            // CONTINUOUS STEERING CHECKING
            //Compute steering based on gyro and drive motors 
            let total_steering = self.gyro_sensor.get_value0()? - starting_pos;
            let mut base_speed = match trip_status {
                TripDistance::RUNNING => 50,
                TripDistance::CLOSE => 20,
                _ => 0,
            };

            let balance: i32 = num::clamp(total_steering, -base_speed, base_speed);

            //Swap if driving backward
            if distance_cm < 0.0 {
                base_speed = -base_speed;
            }

            //adjust the dutycycle of the motors to move left or right for the correction
            let dutycycle_l = base_speed - balance;
            let dutycycle_r = base_speed + balance;
            self.motor_l.set_duty_cycle_sp(dutycycle_l)?;
            self.motor_r.set_duty_cycle_sp(dutycycle_r)?;
            /*println!("Status {:?}", trip_status);
            println!("Dutycycle L: {}, R: {}", dutycycle_l, dutycycle_r);
            println!("Balance: {}, Basespeed:{}", balance, base_speed);
            println!("Gyro diff: {}\n\n", total_steering);*/

            thread::sleep(time::Duration::from_millis(20));
        }
        self.motor_l.stop()?;
        self.motor_r.stop()?;

        self.motor_l.wait_until_not_moving(None);
        self.motor_r.wait_until_not_moving(None);


        println!("Distance moved {}",self.tacho_to_radians(self.motor_l.get_position()?)*self.wheel_radius_cm);
        Ok(())
    }

    /// correct the angle after rotation using gyro
    pub fn reset_angle(&mut self) -> Ev3Result<()> {
 
        thread::sleep(time::Duration::from_millis(500));
        let mut gyro_angle = self.gyro_sensor.get_value0()?; 
        println!("\n\nAbsolute gyro angle {}", gyro_angle);
        if gyro_angle < 0
        {
            gyro_angle = gyro_angle + 360;
        }
        let diff = (gyro_angle + 45) % 90;
        let rel_rotation = diff - 45;
        println!("Rel_rot computed {}\n\n", rel_rotation);
        self.rotate_angle(-rel_rotation)?;
        Ok(())
    }

    /// rotate the motors for a angle using gyro
    /// if angle > 0 it rotates clockwise else it rotates anticlockwise
    pub fn rotate_angle(&mut self, angle: i32) -> Ev3Result<()> {
        if angle !=0
        {
           println!("correct angle {}", angle);
           // get the starting position of the gyro
           let starting_pos = self.gyro_sensor.get_value0()?;

           if angle > 0 {
             //rotate right ploarities
             self.motor_l.set_polarity(LargeMotor::POLARITY_NORMAL)?;
             self.motor_r.set_polarity(LargeMotor::POLARITY_INVERSED)?;
           } else {
             //rotate left ploarities
             self.motor_l.set_polarity(LargeMotor::POLARITY_INVERSED)?;
             self.motor_r.set_polarity(LargeMotor::POLARITY_NORMAL)?;
           }

           // run the motors
           self.motor_l.run_direct()?;
           self.motor_r.run_direct()?;

           self.motor_l.set_duty_cycle_sp(20)?;
           self.motor_r.set_duty_cycle_sp(20)?;

           //rotate untill we reach the angle w.r.t gyro
           while
           (self.gyro_sensor.get_value0()? - starting_pos).abs() < (angle.abs() -2)
           {
               thread::sleep(time::Duration::from_millis(50));
           }
           
           //stop the motors as soon as we reach the limit
           self.motor_l.stop()?;
           self.motor_r.stop()?;
           self.motor_l.wait_until_not_moving(None);
           self.motor_r.wait_until_not_moving(None);

           // check for the error in angle out of the range and correct the rotation 
           if (self.gyro_sensor.get_value0()? - starting_pos).abs() <= (angle.abs()-2) && (self.gyro_sensor.get_value0()? - starting_pos).abs() >= (angle.abs()+2)
           {
              self.rotate_angle((self.gyro_sensor.get_value0()? - starting_pos)-angle)?;
           }

           thread::sleep(time::Duration::from_millis(100));
           let final_angle = self.gyro_sensor.get_value0()?;
           println!("Final angle {}", final_angle);
        }
        Ok(())
    }
    pub fn stop(&self) -> Ev3Result<()> {
        self.motor_l.stop()?;
        self.motor_r.stop()?;
        Ok(())
    }
}
