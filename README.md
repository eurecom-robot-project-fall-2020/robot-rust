# Ravattino

## Description
Ravattino is a Rust robot project on the Lego EV3dev platform.
The goal is to move balls from one box to the other one.

Our website to show the robot can be found at [this address](https://eurecom-robot-project-fall-2020.gitlab.io/website-robot-specs/)

The rules of the competition can be found [on Eurecom's OS website](http://soc.eurecom.fr/OS/projects_fall2020.html).

## Building and running
Make sure to use the Debian Stretch on your Lego EV3

commands to run cross-compilation: 

`docker build . -t pixix4/ev3dev-rust-cross --no-cache`

`docker run -it --rm -v "$PWD:/build/" -w /build pixix4/ev3dev-rust-cross`

Then, inside the container's terminal, run: 
`cargo build --release --target armv5te-unknown-linux-gnueabi`

find the new file at `target/armv5te-unknown-linux-gnueabi/release/robot-rust`

## Trivia
The name of the robot, for pronunciation purposes, is written `/ravatː'ino/` in IPA.
